#!/usr/bin/env lua
require "cea608"

-- Default = x_x_x_... {CR} +00:00:01:00 ...
-- Initial space = x_{}x_{}x_{}... {CR} +00:00:01:00 ...
-- "\" final = ... {CR} ...

local portal_text = { [[
 Forms FORM-29827281-12: \
 Test Assessment Report


 This was a triumph.
 I'm making a note here: \
 HUGE SUCCESS.
 It's hard to overstate \
 my satisfaction.
 Aperture Science
 We do what we must \
 because we can.
 For the good of all of us.
Except the ones who are dead. \

But there's no sense crying \
over every mistake. \
You just keep on trying \
till you run out of cake. \
And the Science gets done. \
And you make a neat gun. \
For the people who are \
 still alive.
]], [[
Forms FORM-55551-5: \
Personnel File Addendum:

 Dear <<Subject Name Here>>, \

 I'm not even angry.
 I'm being so sincere right now.
 Even though you broke my heart. \
And killed me.
And tore me to pieces.
And threw every piece         >
 into a fire.
As they burned it hurt because \
I was so happy for you!
Now these points of data \
make a beautiful line. \
And we're out of beta. \
We're releasing on time. \
So I'm GLaD. I got burned. \
Think of all the things we    >
learned \
for the people who are \
 still alive.
]], [[
Forms FORM-55551-6: \
Personnel File Addendum       >
Addendum:

 One last thing: \

 Go ahead and leave me.
 I think I prefer to stay      >
 inside.
 Maybe you'll find someone else \
 to help you.
 Maybe Black Mesa... \
 THAT WAS A JOKE. FAT CHANCE.
 Anyway, this cake is great. \
It's so delicious and moist.
Look at me still talking \
when there's Science to do. \
When I look out there, \
it makes me GLaD I'm not you. \
I've experiments to run. \
There is research to be done. \
On the people who are \
 still alive.
]], [[
 \
 \
 \
PS: And believe me I am \
still alive.
PPS: I'm doing Science and I'm \
still alive.
PPPS: I feel FANTASTIC and I'm \
still alive. \
 \
FINAL THOUGHT: \
While you're dying I'll be \
still alive. \
 \
FINAL THOUGHT PS: \
And when you're dead I will be \
still alive. \

 \
STILL ALIVE.
]] }

local function generate(text, mode, cursor_on)
  --io.write(string.rep("\xff\xff", 2))
  local istxt, isru = true, false

  local function write_cc_clear()
    if istxt then
      io.write(cea608.TR, cea608.TR)
    else
      io.write(cea608.EDM, cea608.EDM)
    end
  end

  if type(mode) == "string" then
    mode = mode:upper()
    if mode:sub(1, 2) == "RU" then
      istxt, isru = false, true
    end
  end

  write_cc_clear()

  if cursor_on then
    if istxt then
      io.write(cea608.RTD, cea608.RTD)
    else
      io.write(string.rep(cea608[mode:sub(1, 3)] or cea608.RU2, 2))
      io.write(cea608.CR, cea608.CR)
    end
    io.write(string.rep(cea608.PAC(15, "Y"), 2))
    io.write(" \0", cea608["_"], string.rep("\0\0", 6))
  end

  local skipbreak, cursor_on_last_line = false, true
  for l in text:gmatch("(.-)\n") do
    if l == "$" then
      io.write(string.rep("\0\0", 30*2-6))
      write_cc_clear()
      l = ""
    else
      if cursor_on and cursor_on_last_line then
        io.write(cea608.BS, cea608.BS)
        cursor_on_last_line = false
      end
      if istxt then
        if not skipbreak then
          io.write(cea608.RTD, cea608.RTD)
        end
        if cursor_on then
          io.write(cea608.CR, cea608.CR)
        end
      end
    end

    local l2 = l:gsub(" *[>\\]$", ""):gsub("^ ", "")

    if cursor_on or l2 ~= "" then
      if isru then
        io.write(string.rep(cea608[mode:sub(1, 3)] or cea608.RU2, 2))
        io.write(cea608.CR, cea608.CR)
        io.write(string.rep(cea608.PAC(15, "Y"), 2))
      elseif l ~= "" then
        io.write(string.rep(cea608.PAC(15, "Y"), 2))
      end
      if cursor_on then
        io.write(" \0", cea608["_"], "\0\0")
        cursor_on_last_line = true
      end
    end

    if cursor_on then
      local count_char = 0
      for w in l2:gmatch(".") do
        count_char = count_char + 1
        if count_char <= 30 then
          io.write(cea608.BS, cea608.BS)
        else
          io.write("\0\0")
        end
        io.write(w)
        if count_char < 30 then
          io.write(" ", cea608["_"])
        else
          io.write("\0\0\0")
          cursor_on_last_line = false
        end
        if l:sub(1, 1) == " " then
          io.write("\0\0")
        end
      end
    elseif l:sub(1, 1) == " " then
      io.write("" .. l2:gsub(".", "%0\0\0\0\0\0"))
    else
      io.write("" .. l2:gsub(".", "%0\0\0\0"))
    end

    if l:find(" *>$") then
      if cursor_on then
        io.write(cea608.BS, cea608.BS)
        cursor_on_last_line = false
      end
      l = l:match(" *>$")
      io.write(l)
      if #l % 2 > 0 then
        io.write("\0")
      end
    end

    skipbreak = l:find(" *[\\>]$") ~= nil

    if istxt and not cursor_on then
      io.write(cea608.CR, cea608.CR)
    end
    if not skipbreak then
      io.write(string.rep("\0\0", 5 + 8 * 4))
    end
  end

  io.write(string.rep("\0\0", 8*12 + 7))
  write_cc_clear()
end

local function main()
  local text_id, mode, cursor_on
  if #arg >= 1 then
    for i = 1, #arg do
      if arg[i] == "-c" then
        cursor_on = true
      elseif #arg[i] == 1 then
        text_id = tonumber(arg[i])
        if portal_text[text_id] == nil then
          io.stderr:write(string.format(
            "%s: Número selecionado não existe (%s).\n",
            arg[0], arg[i]))
          os.exit(1)
        end
      else
        mode = arg[i]
      end
    end
  end
  if not text_id then
    generate(table.concat(portal_text, "$\n"), mode, cursor_on)
  else
    generate(portal_text[text_id], mode, cursor_on)
  end
end

main()
