-- CEA-608 module

cea608 = {
  FY = "\x11\x2a",

  ["_"] = "\x13\x2d",

  RCL = "\x14\x20",
  BS  = "\x14\x21",
  RU2 = "\x14\x25",
  RU3 = "\x14\x26",
  RU4 = "\x14\x27",
  RDC = "\x14\x29",
  TR  = "\x14\x2a",
  RTD = "\x14\x2b",
  EDM = "\x14\x2c",
  CR  = "\x14\x2d",
  ENM = "\x14\x2e",
  EOC = "\x14\x2f",

  PAC = function (row, col_color, underline)
    row = (row - 1) % 15 + 1

    local first = 0x10
    if row <= 4 then
      first = math.floor((row - 1) / 2) + 0x11
    elseif row >= 12 then
      first = math.floor((row - 12) / 2) + 0x13
    elseif row ~= 11 then
      first = math.floor((row - 5) / 2) + 0x15
    end
    if col_color == "Y" then
      col_color = 0x4a
    else
      col_color = 2 * math.floor(col_color / 4) + 0x50
    end
    col_color = 0x20 * ((row - (row <= 11 and 1 or 0)) % 2) + col_color
    if underline then
      col_color = col_color + 1
    end

    return string.char(first, col_color)
  end,
}
